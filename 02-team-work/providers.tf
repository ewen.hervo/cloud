provider "aws" {
  region = "eu-west-3"
  default_tags {
    tags = {
      "owner"    = "groupe_7"
      "practice" = "cloud-and-devops"
    }
  }
}
