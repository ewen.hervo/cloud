provider "aws" {
  region = "eu-west-3" #Paris
  default_tags {
    tags = {
      "owner"    = "formation-terraform"
      "practice" = "cloud-and-devops"
    }
  }
}
