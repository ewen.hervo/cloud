#AWS
variable "aws_account_id" {
  description = "Groupe_7"
  type = string
  validation {
    condition = length(var.aws_account_id) == 12
    error_message = "account id must be 12 numeric digit long"
  }
  default = "590183689411"
}


variable "prefix_name" {
  default = "Groupe_7"
}